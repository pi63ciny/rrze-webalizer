let domain = document.getElementById("daily_usage_graph").firstElementChild.getAttribute("src").match(".*(?=\\/)\\/");


let i;
for (i = 0; i < document.getElementsByClassName("stats_footer_tbody").length; i++) {

    let oldlink = document.getElementsByClassName("stats_footer_tbody").item(i).lastElementChild.lastElementChild.lastElementChild.getAttribute("href").match("\\w+|\\d")[0];


    let newLink = domain + oldlink + ".shtml";


    document.getElementsByClassName("stats_footer_tbody").item(i).lastElementChild.lastElementChild.lastElementChild.setAttribute("href", newLink);

}

