let links = document.getElementsByClassName("all_items_tr");
let pageurl = document.getElementsByClassName("page_header_div")[0].firstElementChild.textContent.split(" ")[1];
let urlFirstPart = "https://statistiken.rrze.fau.de/webserver/infolog/" + pageurl + "/";

let i;
for (i = 0; i < links.length; i++) {
    let oldNode = links[i].children.item(0);
    let urlImagePart = oldNode.firstElementChild.getAttribute("href");
    let newURL = urlFirstPart+urlImagePart;
    oldNode.children.item(0).setAttribute("href",newURL);
}
