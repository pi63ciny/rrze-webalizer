<?php

namespace RRZE\Webalizer\Config;

defined('ABSPATH') || exit;

/**
 * Gibt der Name der Option zurück.
 * @return array [description]
 */
function getOptionName()
{
    return 'rrze_webalizer';
}

/**
 * Gibt die Einstellungen des Menus zurück.
 * @return array [description]
 */
function getMenuSettings()
{
    return [
        'page_title'    => __('RRZE Webalizer', 'rrze-webalizer'),
        'menu_title'    => __('RRZE Webalizer', 'rrze-webalizer'),
        'capability'    => 'manage_options',
        'menu_slug'     => 'rrze-webalizer',
        'title'         => __('RRZE Webalizer Settings', 'rrze-webalizer'),
    ];
}

/**
 * Gibt die Einstellungen der Inhaltshilfe zurück.
 * @return array [description]
 */
function getHelpTab()
{
    return [
        [
            'id'        => 'rrze-webalizer-help',
            'content'   => [
                '<p>' . __('Here comes the Context Help content.', 'rrze-webalizer') . '</p>'
            ],
            'title'     => __('Overview', 'rrze-webalizer'),
            'sidebar'   => sprintf('<p><strong>%1$s:</strong></p><p><a href="https://blogs.fau.de/webworking">RRZE Webworking</a></p><p><a href="https://github.com/RRZE Webteam">%2$s</a></p>', __('For more information', 'rrze-webalizer'), __('RRZE Webteam on Github', 'rrze-webalizer'))
        ]
    ];
}

/**
 * Gibt die Einstellungen der Optionsbereiche zurück.
 * @return array [description]
 */
function getSections()
{
    return [
        [
            'id'    => 'basic',
            'title' => __('Basic Settings', 'rrze-webalizer')
        ],
        [
            'id'    => 'advanced',
            'title' => __('Advanced Settings', 'rrze-webalizer')
        ]
    ];
}

/**
 * Gibt die Einstellungen der Optionsfelder zurück.
 * @return array [description]
 */
function getFields()
{
    return [
        'basic' => [
            [
                'name'              => 'text_input',
                'label'             => __('Text Input', 'rrze-webalizer'),
                'desc'              => __('Text input description.', 'rrze-webalizer'),
                'placeholder'       => __('Text Input placeholder', 'rrze-webalizer'),
                'type'              => 'text',
                'default'           => 'Title',
                'sanitize_callback' => 'sanitize_text_field'
            ],
            [
                'name'              => 'number_input',
                'label'             => __('Number Input', 'rrze-webalizer'),
                'desc'              => __('Number input description.', 'rrze-webalizer'),
                'placeholder'       => '5',
                'min'               => 0,
                'max'               => 100,
                'step'              => '1',
                'type'              => 'number',
                'default'           => 'Title',
                'sanitize_callback' => 'floatval'
            ],
            [
                'name'        => 'textarea',
                'label'       => __('Textarea Input', 'rrze-webalizer'),
                'desc'        => __('Textarea description', 'rrze-webalizer'),
                'placeholder' => __('Textarea placeholder', 'rrze-webalizer'),
                'type'        => 'textarea'
            ],
            [
                'name'  => 'checkbox',
                'label' => __('Checkbox', 'rrze-webalizer'),
                'desc'  => __('Checkbox description', 'rrze-webalizer'),
                'type'  => 'checkbox'
            ],
            [
                'name'    => 'multicheck',
                'label'   => __('Multiple checkbox', 'rrze-webalizer'),
                'desc'    => __('Multiple checkbox description.', 'rrze-webalizer'),
                'type'    => 'multicheck',
                'default' => [
                    'one' => 'one',
                    'two' => 'two'
                ],
                'options'   => [
                    'one'   => __('One', 'rrze-webalizer'),
                    'two'   => __('Two', 'rrze-webalizer'),
                    'three' => __('Three', 'rrze-webalizer'),
                    'four'  => __('Four', 'rrze-webalizer')
                ]
            ],
            [
                'name'    => 'radio',
                'label'   => __('Radio Button', 'rrze-webalizer'),
                'desc'    => __('Radio button description.', 'rrze-webalizer'),
                'type'    => 'radio',
                'options' => [
                    'yes' => __('Yes', 'rrze-webalizer'),
                    'no'  => __('No', 'rrze-webalizer')
                ]
            ],
            [
                'name'    => 'selectbox',
                'label'   => __('Dropdown', 'rrze-webalizer'),
                'desc'    => __('Dropdown description.', 'rrze-webalizer'),
                'type'    => 'select',
                'default' => 'no',
                'options' => [
                    'yes' => __('Yes', 'rrze-webalizer'),
                    'no'  => __('No', 'rrze-webalizer')
                ]
            ]
        ],
        'advanced' => [
            [
                'name'    => 'color',
                'label'   => __('Color', 'rrze-webalizer'),
                'desc'    => __('Color description.', 'rrze-webalizer'),
                'type'    => 'color',
                'default' => ''
            ],
            [
                'name'    => 'password',
                'label'   => __('Password', 'rrze-webalizer'),
                'desc'    => __('Password description.', 'rrze-webalizer'),
                'type'    => 'password',
                'default' => ''
            ],
            [
                'name'    => 'wysiwyg',
                'label'   => __('Advanced Editor', 'rrze-webalizer'),
                'desc'    => __('Advanced Editor description.', 'rrze-webalizer'),
                'type'    => 'wysiwyg',
                'default' => ''
            ],
            [
                'name'    => 'file',
                'label'   => __('File', 'rrze-webalizer'),
                'desc'    => __('File description.', 'rrze-webalizer'),
                'type'    => 'file',
                'default' => '',
                'options' => [
                    'button_label' => __('Choose an Image', 'rrze-webalizer')
                ]
            ]
        ]
    ];
}


/**
 * Gibt die Einstellungen der Parameter für Shortcode für den klassischen Editor und für Gutenberg zurück.
 * @return array [description]
 */

function getShortcodeSettings(){
	return [
		'block' => [
            'blocktype' => 'rrze-webalizer/SHORTCODE-NAME', // dieser Wert muss angepasst werden
			'blockname' => 'SHORTCODE-NAME', // dieser Wert muss angepasst werden
			'title' => 'SHORTCODE-TITEL', // Der Titel, der in der Blockauswahl im Gutenberg Editor angezeigt wird
			'category' => 'widgets', // Die Kategorie, in der der Block im Gutenberg Editor angezeigt wird
            'icon' => 'admin-users',  // Das Icon des Blocks
            'show_block' => 'content', // 'right' or 'content' : Anzeige des Blocks im Content-Bereich oder in der rechten Spalte
			'message' => __( 'Find the settings on the right side', 'rrze-webalizer' ) // erscheint bei Auswahl des Blocks, wenn "show_block" auf 'right' gesetzt ist
		],
		'Beispiel-Textfeld-Text' => [
			'default' => 'ein Beispiel-Wert',
			'field_type' => 'text', // Art des Feldes im Gutenberg Editor
			'label' => __( 'Beschriftung', 'rrze-webalizer' ),
			'type' => 'text' // Variablentyp der Eingabe
		],
		'Beispiel-Textfeld-Number' => [
			'default' => 0,
			'field_type' => 'text', // Art des Feldes im Gutenberg Editor
			'label' => __( 'Beschriftung', 'rrze-webalizer' ),
			'type' => 'number' // Variablentyp der Eingabe
		],
		'Beispiel-Textarea-String' => [
			'default' => 'ein Beispiel-Wert',
			'field_type' => 'textarea',
			'label' => __( 'Beschriftung', 'rrze-webalizer' ),
			'type' => 'string',
			'rows' => 5 // Anzahl der Zeilen 
		],
		'Beispiel-Radiobutton' => [
			'values' => [
				'wert1' => __( 'Wert 1', 'rrze-webalizer' ), // wert1 mit Beschriftung
				'wert2' => __( 'Wert 2', 'rrze-webalizer' )
			],
			'default' => 'DESC', // vorausgewählter Wert
			'field_type' => 'radio',
			'label' => __( 'Order', 'rrze-webalizer' ), // Beschriftung der Radiobutton-Gruppe
			'type' => 'string' // Variablentyp des auswählbaren Werts
		],
		'Beispiel-Checkbox' => [
			'field_type' => 'checkbox',
			'label' => __( 'Beschriftung', 'rrze-webalizer' ),
			'type' => 'boolean',
			'default'   => true // Vorauswahl: Haken gesetzt
        ],
        'Beispiel-Toggle' => [
            'field_type' => 'toggle',
            'label' => __( 'Beschriftung', 'rrze-webalizer' ),
            'type' => 'boolean',
            'default'   => true // Vorauswahl: ausgewählt
        ],
        'Beispiel-Select' => [
			'values' => [
				'wert1' => __( 'Wert 1', 'rrze-webalizer' ),
				'wert2' => __( 'Wert 2', 'rrze-webalizer' )
			],
			'default' => 'wert1', // vorausgewählter Wert: Achtung: string, kein array!
			'field_type' => 'select',
			'label' => __( 'Beschrifung', 'rrze-webalizer' ),
			'type' => 'string' // Variablentyp des auswählbaren Werts
		],
        'Beispiel-Multi-Select' => [
			'values' => [
				'wert1' => __( 'Wert 1', 'rrze-webalizer' ),
				'wert2' => __( 'Wert 2', 'rrze-webalizer' ),
				'wert3' => __( 'Wert 2', 'rrze-webalizer' )
			],
			'default' => ['wert1','wert3'], // vorausgewählte(r) Wert(e): Achtung: array, kein string!
			'field_type' => 'multi_select',
			'label' => __( 'Beschrifung', 'rrze-webalizer' ),
			'type' => 'array',
			'items'   => [
				'type' => 'string' // Variablentyp der auswählbaren Werte
			]
        ]
    ];
}

