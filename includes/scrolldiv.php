<?php

use function RRZE\Webalizer\getFromPage;
$path = "//*[@id=\"content\"]";
$context = $context = array(
    "http" => array(
        "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
        "follow_location" => "0"
    )
);

$domain = $_SERVER['SERVER_NAME'];
if ($domain == "localhost"){
    _e("Not working with localhost", "rrze-webalizer");
    return;
}

$show = "usage_";
$year = date("Y");
$month = date("m");

$doc = new DOMDocument();
$doc->loadHTML(getFromPage($path, $context, $domain,$show, $year, $month));



if ( @!$contentChildren = $doc->getElementById("content")->childNodes){
    _e("There is no statistic available for your WordPress-Instance", "rrze-webalizer");
    return;
}

for ($i=0;$i<$contentChildren->count();$i++){
    if ($contentChildren->item($i)->nodeName == "table") {
        echo "<div class=\"table-wrapper\">";
        echo "<div class=\"scrollable\">";
        echo $doc->getElementById("content")->childNodes->item($i)->C14N();
        echo "</div>";
        echo "</div>";
    } else if ($contentChildren->item($i)->hasChildNodes() == true) {
        $contentChildren2 = $contentChildren->item($i)->childNodes;
        for ($i2=0;$i2<$contentChildren2->count();$i2++){
            if ($doc->getElementById("content")->childNodes->item($i)->childNodes->item($i2)->nodeName == "table"){
                echo "<div class=\"table-wrapper\">";
                echo "<div class=\"scrollable\">";
                echo $doc->getElementById("content")->childNodes->item($i)->childNodes->item($i2)->C14N();
                echo "</div>";
                echo "</div>";
            } else {
                echo $doc->getElementById("content")->childNodes->item($i)->childNodes->item($i2)->C14N();
            }
        }
    } else {
        echo $doc->getElementById("content")->childNodes->item($i)->C14N();
    }

}


