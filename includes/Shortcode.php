<?php

namespace RRZE\Webalizer;

defined('ABSPATH') || exit;

use DOMDocument;
use DOMNode;
use DOMXPath;
use XMLReader;
use function RRZE\Webalizer\Config\getShortcodeSettings;



/**
 * Shortcode
 */
class Shortcode
{

    /**
     * Der vollständige Pfad- und Dateiname der Plugin-Datei.
     * @var string
     */
    protected $pluginFile;

    /**
     * Settings-Objekt
     * @var object
     */
    private $settings = '';

    /**
     * Variablen Werte zuweisen.
     * @param string $pluginFile Pfad- und Dateiname der Plugin-Datei
     */
    public function __construct($pluginFile, $settings)
    {
        $this->pluginFile = $pluginFile;
        $this->settings = getShortcodeSettings();
    }

    /**
     * Er wird ausgeführt, sobald die Klasse instanziiert wird.
     * @return void
     */
    public function onLoaded()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_shortcode('rrze-webalizer', [$this, 'rrzewebalizer']);
        add_action('wp_dashboard_setup', [$this, 'my_custom_dashboard_widgets']);
    }

    function my_custom_dashboard_widgets() {
        global $wp_meta_boxes;
        wp_enqueue_style( 'dashboard_widget', plugin_dir_url( __FILE__ ) . 'dashboard_widget.css', array(), '1.0' );

        wp_add_dashboard_widget('rrze_webalizer_widget', 'RRZE-Webalizer', [$this,'rrze_webalizer_widget']);
    }

    function rrze_webalizer_widget (){


        include_once 'scrolldiv.php';
        echo "<script src='/wp-content/plugins/rrze-webalizer/js/changeLinks.js'></script>";
    }

    /**
     * Enqueue der Skripte.
     */

    public function enqueueScripts()
    {
        wp_register_style('rrze-webalizer-shortcode', plugins_url('assets/css/shortcode.css', plugin_basename($this->pluginFile)));
        wp_register_script('rrze-webalizer-shortcode', plugins_url('assets/js/shortcode.js', plugin_basename($this->pluginFile)));
    }


    public function rrzewebalizer($atts) {
        $domain = $_SERVER['SERVER_NAME'];
        $show = "usage_";

        if (isset($atts['domain'])){
            $domain = $atts['domain'];
        }

        if (!isset($atts['show'])){
            $atts['show'] = "usage";
        }
        if (isset($atts['show'])) {
            switch ($atts['show']){
                case "usage":
                    $show = "usage_";
                    break;
                case "useragents":
                    $show = "agent_";
                    break;
                case "directlinks":
                    $show = "dl_";
                    break;
                case "delta":
                    $show = "url_delta_";
                    break;
                }
        }

        if (!isset($atts['year'])){
            $year = date("Y");
        } else {
            $year = $atts['year'];
        }
        if (!isset($atts['month'])) {
            $month = date("m");
        } else {
            $month = $atts['month'];
        }
        if (!isset($atts['day'])) {
            $day = date("d");
        } else {
            $day = $atts['day'];
        }

        $context = array(
                    "http" => array(
                        "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
                        "follow_location" => "0"
                        )
                    );
        $allCount = 0;
        if ( $atts["show"] == "all") {
            while ($allCount < 4) {
                switch ($allCount) {
                    case 0:
                        $atts["show"] = "usage";
                        break;
                    case 1:
                        $atts["show"] = "useragents";
                        break;
                    case 2:
                        $atts["show"] = "directlinks";
                        break;
                    case 3:
                        $atts["show"] = "delta";
                        break;
                }
                if ( @$atts["show"] != "delta" && @$atts["show"] != "directlinks") {
                    echo getFromPage( "//*[@id=\"content\"]",$context, $domain,$show, $year, $month);
                    echo "<script src='/wp-content/plugins/rrze-webalizer/js/embed.js'></script>";
                } else {
                    getFromTabPage($atts,$context,$domain,$atts["show"],$year,$month,$day);
                }
                $allCount++;
            }
        } else {
            if (@$atts["show"] != "delta" && @$atts["show"] != "directlinks") {
                echo getFromPage("//*[@id=\"content\"]", $context, $domain, $show, $year, $month);
                echo "<script src='/wp-content/plugins/rrze-webalizer/js/embed.js'></script>";
            } else {
                getFromTabPage($atts, $context, $domain, $atts["show"], $year, $month, $day);
            }
        }

    }


    public function gutenberg_init() {
        // Skip block registration if Gutenberg is not enabled/merged.
        if ( ! function_exists( 'register_block_type' ) ) {
            return;
        }

        $js = '../assets/js/gutenberg.js';
        $editor_script = $this->settings['block']['blockname'] . '-blockJS';

        wp_register_script(
            $editor_script,
            plugins_url( $js, __FILE__ ),
            array(
                'wp-blocks',
                'wp-i18n',
                'wp-element',
                'wp-components',
                'wp-editor'
            ),
            filemtime( dirname( __FILE__ ) . '/' . $js )
        );

        wp_localize_script( $editor_script, 'blockname', $this->settings['block']['blockname'] );

        register_block_type( $this->settings['block']['blocktype'], array(
            'editor_script' => $editor_script,
            'render_callback' => [$this, 'shortcodeOutput'],
            'attributes' => $this->settings
            )
        );

        wp_localize_script( $editor_script, $this->settings['block']['blockname'] . 'Config', $this->settings );
    }
}

function getFromTabPage($atts, $context, $domain,$show, $year, $month, $day){
    $date = new \DateTime();
    $date->setDate($year, $month, $day);
    if ($show == "delta") {
        $showURL = "/url_delta_";
    } else {
        $showURL = "/dl_";
        $day = "";
    }
    $webAddress = "https://statistiken.rrze.fau.de/webserver/infolog/" . $domain . $showURL . $year . $month . $day . ".tab";

    $count = 0;

    if (!isset($atts["day"])) {
        while (true) {
            $source = @file_get_contents($webAddress, false, stream_context_create($context));
            $doc = new DOMDocument();
            @$doc->loadHTML($source);

            if (@$doc->getElementsByTagName("h1")->item(0)->nodeValue == "Multiple Choices") {
                $tempYear = $date->format("Y");
                $tempMonth = $date->format("m");
                $tempDay = $date->format("d");
                $date->sub(new \DateInterval('P1D'));
                if ($show == "directlinks") {
                    $tempDay = "";
                }
                $webAddress = "https://statistiken.rrze.fau.de/webserver/infolog/" . $domain . $showURL . $tempYear . $tempMonth . $tempDay . ".tab";
                $count = $count + 1;
            } else {
                if ($count > 0) {
                    $date->add(new \DateInterval('P1D'));
                }
                break;
            }
        }
    } else {
        $source = @file_get_contents($webAddress, false, stream_context_create($context));
        $doc = new DOMDocument();
        @$doc->loadHTML($source);
    }


    $tempYear = $date->format("Y");
    $tempMonth = $date->format("m");
    $tempDay = $date->format("d");
    if ($show == "directlinks") {
        $tempDay = "";
    }
    echo "<h3>$domain$showURL$tempYear$tempMonth$tempDay</h3>";
    if (!@preg_match_all("~\d+\t\d+\s+\/.+~", $doc->getElementsByTagName("p")->item(0)->nodeValue, $matches)){
        if (@$doc->getElementsByTagName("h1")->item(0)->nodeValue == "Moved Permanently"){
            _e("Requested URL not found", "rrze-webalizer");
        } else {
            _e("Page is empty", "rrze-webalizer");
        }
    }
    foreach ($matches[0] as $match) {
        echo "<p>$match</p>";
    }
}

function replace_img_src(DOMDocument $doc, $new_src_url) {

    $tags = $doc->getElementsByTagName("img");

    foreach ($tags as $tag){
        $old_src = $tag->getAttribute('src');
        $new_src = $new_src_url . $old_src;
        $tag->setAttribute('src', $new_src);
    }
    $imgHTML = $doc->saveHTML();
    return $imgHTML;
}

function getFromPage($path, $context, $domain,$show, $year, $month){

    $webAddress = "https://statistiken.rrze.fau.de/webserver/infolog/".$domain."/".$show.$year.$month.".shtml";

    if(!$source = @file_get_contents($webAddress,false,stream_context_create($context))){
        _e("WARNING: You are NOT inside the University-Network!", "rrze-webalizer");
    } //download the page

    $doc = new DOMDocument;
    // load the html source from a string

    @$doc->loadHTML($source);
    $xpath = new DOMXPath($doc);

    $newdoc = new DOMDocument;
    @$newdoc->loadHTML($doc->saveHTML($xpath->query($path)->item(0)));

    $htmlString = replace_img_src($newdoc,"https://statistiken.rrze.fau.de/webserver/infolog/$domain/");
    @$doc->loadHTML($htmlString);
    if (@$doc->getElementsByTagName("h1")->item(0)->nodeValue == "Moved Permanently" || @$doc->getElementsByTagName("h1")->item(0)->nodeValue == "Multiple Choices") {
        return "<h1>Site not found</h1>";
    } else {
        return $htmlString;

    }

}

