<?php

namespace RRZE\Webalizer;

defined('ABSPATH') || exit;

/**
 * Exception-Klasse
 */
class Exception extends \Exception
{
}
