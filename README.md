## RRZE-Webalizer ##
Plugin to display statistics of statistiken.rrze.fau.de.  
  
### Shortcode ###
Shortcode parameter: `domain="www.fau.de"` `show="usage","useragents","directlinks","delta" or "all"` `year="2020"` `month="03"` `day="24"`  
If no year, month and day is set, the current date will be used
  
Shortcode example: `[rrze-webalizer domain="www.fau.de" show="usage"]`


### Dashboard-Widget ###
  
Displays the usage of the current WordPress-Instance